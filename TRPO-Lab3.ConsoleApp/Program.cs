﻿using System;
using TRPO_Lab3.Lib;


namespace TRPO_Lab3.ConsoleApp

{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите h");
            string strH = Console.ReadLine();
            double h = Convert.ToDouble(strH);
            Console.WriteLine("Введите R");
            string strR = Console.ReadLine();
            double R = Convert.ToDouble(strR);
            if (h <= 0 || R <= 0)
            {
                Console.WriteLine("Введены неверные значения");
                return;
            }

            BallSeg area = new BallSeg(R, h);
            Console.WriteLine("S: {0}", area.Area());
        }
    }
}