using NUnit.Framework;
using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void TestAreEqual()
        {
            const double r = 1;
            const double h = 2;
            const double expected = 12.566370614359172;
            double result = new BallSeg(r, h).Area();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void TrowTest1()
        {

            const double r = -1;
            const double h = -1;

            Assert.Throws<ArgumentException>(() => new BallSeg(r, h).Area());
        }
        [Test]
        public void TrowTest2()
        {

            const double r = -1;
            const double h = 1;

            Assert.Throws<ArgumentException>(() => new BallSeg(r, h).Area());
        }
        [Test]
        public void TrowTest3()
        {

            const double r = 1;
            const double h = -1;

            Assert.Throws<ArgumentException>(() => new BallSeg(r, h).Area());
        }
    }
}
