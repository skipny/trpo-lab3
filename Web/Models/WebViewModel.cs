using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class WebViewModel
    {
        public double R { get; set; }
        public double H { get; set; }
        public double S 
        {
            get
            {
                return (new TRPO_Lab3.Lib.BallSeg(R,H).Area());
            }
        }
    }
}
