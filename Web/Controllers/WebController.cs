using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using Web.Models;

namespace WebApp.Controllers
{
    public class WebController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var viewModel = new WebViewModel();
            viewModel.R = 1;
            viewModel.H = 1;
            return View(viewModel);
        }
        [HttpPost]
        public IActionResult Index(IFormCollection form)
        {
            var viewModel = new WebViewModel();
            if (!form.TryGetValue("R_1", out var str_R) || !form.TryGetValue("H_1", out var str_H))
            {
                str_R = "1";
                str_H = "1";

            }
            if (!Double.TryParse(str_R, out var R) || !Double.TryParse(str_H, out var H))
            {
                R = 1;
                H = 1;
            }
            viewModel.R = R;
            viewModel.H = H;
            return View(viewModel);
        }
    }
}

