﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.ComponentModel;
using TRPO_Lab3.Lib;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
    public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
    }
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private double _R;
        public double R
        {
            get { return _R; }
            set
            {
                _R = value;
                if(_R>0)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res)));
            }
        }

        private double _H;
        public double H
        {
            get { return _H; }
            set
            {      
                _H = value;
                if (_H > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res)));

            }
        }

        public double Res
        {
            get { return new BallSeg(R, H).Area(); }
        }
    }

}
