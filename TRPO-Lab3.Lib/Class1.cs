﻿using System;

namespace TRPO_Lab3.Lib
{
    public class BallSeg 
    {
        private double r;
        private double h;
        public BallSeg(double R, double h)
        {
            this.r = R;
            this.h = h;
            //if (h <= 0 || r <= 0) 1throw new ArgumentException();

        }
        public double Area()
        {
            return 2 * Math.PI * this.r * this.h;
        }
    }
}

